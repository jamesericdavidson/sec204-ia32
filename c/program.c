/*
 * Copyright 2018-2020 James Davidson, Joshua Allinson
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main() {
	char name[255];				// Allow lots of characters for a buffer overflow (?)
	int rnd1, rnd2, score;
	puts("Enter your name:");
	scanf("%s", name);
	srand(time(NULL));
	rnd1 = rand() % 100 + 1;
	rnd2 = rand() % 100 + 1;
	score = rnd1 + rnd2;
	score = score / 2;
	printf("%s: %d", name, score);		// No choice but to use printf for an int
	return 0;
}
