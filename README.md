# IA32

Coursework for SEC204 - Computer Architecture and Low Level Programming

*Create a vulnerable IA32 Assembly program*

---

* The assembly program was designed to be exploited - the goal being to achieve the maximum score of 100
    * The program was discussed in a report, detailing the exploits and how to mitigate them
* C programs were written, compiled, dumped and reverse engineered to better understand the instructions `gcc` uses

```
Copyright 2018-2020 James Davidson, Joshua Allinson

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```
