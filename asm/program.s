# Copyright 2018-2020 James Davidson, Joshua Allinson
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

.section .data
prompt:
	.asciz "Enter your name:"
allowString:
	.asciz "%s"
modulus:
	.int 101
mean:
	.int 2
result:
	.asciz "%s: %d"

.section .bss
	.lcomm name, 1

.section .text
.globl _start
_start:
# Ask user for their name 
pushl	$prompt
call	puts

# Accept user input
# Arguments must be entered in reverse (as they are pushed to the top of the stack)
pushl	$name			# Push contents of stdin input into a buffer
pushl	$allowString		# Allow a string of characters to be entered
call	scanf

# Generate two random numbers and push them to the stack
call	time			# time is used as the seed for srand
pushl	%eax			# srand grabs the value from the top of the stack to use as a seed
call	srand			# srand need only be called once
call	random
pushl	%ecx
call	random

# Calculate the mean
pop	%eax			# Pops the first random number into eax
addl	%ecx, %eax		# Add the value of the second random number (ecx) to eax
movl	mean, %ebx
divl	%ebx			# Divide the result (eax) by 2

# Print user's name and score
pushl	%eax
pushl	$name
pushl	$result
call	printf

call	exit

# Generate a random number between 0-100 using the modulo formula:
# https://stackoverflow.com/questions/2664301/how-does-modulus-divison-work/13710545#13710545
random:
call	rand
xorl	%edx, %edx
movl	modulus, %ebx
divl	%ebx			# Divide the contents of eax by ebx
imull	%ebx			# Multiply eax by ebx
subl	%eax, %ecx		# Subtract ecx from eax
ret
