# Build scripts for Assembly and C

* `32`, `64`
	- Build for 32 or 64 bit (designed for 32 bit)

* `dbuild`, `build`
	- Build with or without dynamic linker (designed for use with dynamic linker)
